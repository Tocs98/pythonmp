var ChannelServerProvider = require('./ChannelServerProvider.js');

//////////////////
var clientChannelServer;

main();

function main() {
	var localCommandList = [];
	var channelName = "";
	clientChannelServer = new ChannelServerProvider();

	var consoleInput = function() {

		input( channelName + ">> ", function(data) {

			var parts = data.trim().split(' ');
			if (parts[0]) parts[0] = parts[0].toUpperCase();

			switch (true) {
				case (parts[0]=="Q"):
					process.exit(0);
					break;

				case (parts[0]=="REG"):

					var server = parts[1];
					channelName = parts[2];
					subscriber = parts[3];
					if (!server ||!channelName  || !subscriber) {
						console.log("Missing server or channel. Formst: >> REG ServerIP ChannelName SubscriberID");
						consoleInput();
					} else {
						clientChannelServer.registerToChannel(server, channelName, subscriber, function() {
							consoleInput();
						});
					}
					break;

				case (parts[0]=="UNREG"):
					var socket =  clientChannelServer.channelServer.socket;
					socket.destroy();
					channelName= "";
					consoleInput();
					break;

				default:
					var serverIP =  clientChannelServer.channelServer;
					var socket = clientChannelServer.channelServer.socket;
					if (socket) {
						socket.write(data, function() {});
						socket.once('data', function(d) {
							console.log("<< " + d);
					    	});
					} else {
						console.log("No connection found for SLServer " + serverIP);
					}
					setTimeout( consoleInput, 300);
					break;
			}

		});
	};

	console.log("----------------------------")
	console.log("REG IP CHANNEL_NAME SUBSCRIBER - Connect to Server and Register to Channel with a name");
	console.log("UNREG - Disconnect from all SLServers");
	console.log("----------------------------")
	console.log("Q - Exit");
	console.log("----------------------------")

	consoleInput();
}

function input(question, callback) {
	 var stdin = process.stdin, stdout = process.stdout;

	 stdin.resume();
	 stdout.write(question);

	 stdin.once('data', function(data) {
	   data = data.toString().trim();
	   callback(data);
	 });
}
