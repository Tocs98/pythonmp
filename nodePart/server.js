var net = require('net');
var ClientsList = require('./ClientsList.js');

//////////////////
var PORT = 43234;
var ClientsList;

main();

function main() {

	ClientsList = new ClientsList();

	initServer(ClientsList);

	showConsole();

}

function showConsole(){
	var consoleInput = function() {
		input( ">> ", function(data) {
			var parts = data.trim().split(' ');
			if (parts[0]) parts[0] = parts[0].toUpperCase();

			switch (true) {
				case (parts[0]=="Q"):
					process.exit(0);
					break;
				case (parts[0]=="LST"):
					//Hint: use ClientsList.getChannels(); and  ClientsList.getSubscribersDetail();
					if(!parts[1]){
						listAllChannels();
					} else{
						listChannel(parts[1]);
					}

					break;
				case (parts[0]=="SEND"):
					//Hint ClientsList.getSocket(parts[1]);
					let to = parts[1];
					let mes = parts[2];
					sendMessageTo(to, mes);
					break;
				case (parts[0]=="SENDALL"):
					//Hint: use ClientsList.getSubscribersDetail() and ClientsList.send();
					sendAll(parts[1], parts[2]);
					break;
				case (parts[0]=="WHEREIS"):
					if (parts[1]) {
						var details = ClientsList.getSubscriberDetail(parts[1]);
						console.log ( JSON.stringify(  details ) );
					}
					setTimeout( consoleInput, 300);
					break;
				default:
					console.log("Incorrect command");
					consoleInput();
					break;
			}

		});
	};

	console.log("----------------------------")
	console.log("LST - List regsitered CHANNELS and its sockets and stats");
	console.log("LST [CHANNEL] - List regsitered subscribers in the CHANNEL");
	console.log("WHEREIS SUBSCRIBER - List CHANNELS and sockets of subscriber SUBSCRIBER");
	console.log("SEND [SUBSCRIBER] [MESSAGE] - Send MESSAGE to the SUBSCRIBER client identified")
	console.log("SENDALL [CHANNEL] [MESSAGE] - Send MESSAGE to all subscribers in the CHANNEL")
	console.log("----------------------------")
	console.log("Q - Exit");
	console.log("----------------------------")

	consoleInput();
}

function listAllChannels(){
		let clients = ClientsList.getChannels();
		for(let i = 0; i < ClientsList.getChannels().length; i++){
			console.log("Channel "+i + " name " + clients[i]);
		}
		//Hint: use ClientsList.getChannels(); and  ClientsList.getSubscribersDetail();
		showConsole();
}

function listChannel(channel) {
	let subs = ClientsList.getSubscribersDetail(channel);
	console.log(subs);
	for(let i = 0; i < subs.length; ++i){
		console.log(subs[i].subscribers + '\n');
	}


	showConsole();
}

function sendAll(ch, ms){
	 let channel = ClientsList.getSubscribersDetail(ch);
	 ClientsList.send(channel[0].channel, ms); //SEMPRE QUE FAIG UN SEND, EM DONA UN ERROR
																						 /*
																						 this.connectedClients[id_socket].socket.write(data, function() {
																																					 ^

																						TypeError: Cannot read property 'socket' of undefined
																						*/
}

function sendMessageTo(to, mes){
	let id_socket = ClientsList.getSockets(to);
	console.log(id_socket[0]);
	ClientsList.send(id_socket[0].socket, mes);
	showConsole();
}

function initServer(clientsList) {
	net.createServer(function(sock) {
		sock.id = Math.floor(Math.random() * 100000);
			console.log('CONNECTION RECEIVED: ' + sock.remoteAddress +':'+ sock.remotePort);
	    sock.on('data', function(data) {
	    	var _self = this;
	    	console.log('DATA ' + sock.remoteAddress + ': ' + data);
	    	data.toString('utf-8').split('\n').forEach(function(message){
	    		parseData(clientsList, message, _self);
	    	});
	    });
	    sock.on('close', function(data) {
	    	console.log('Connection closed for socket ' + sock.id);
	    	clientsList.removeSocket(sock.id);
	    });
	    sock.on('error', function (err) {
		    console.log("Socket error " + sock.id, err.stack);
		    clientsList.removeSocket(sock.id);
		});
	    clientsList.addClient(null, sock, sock.id);
	}).listen(PORT);
	console.log('Server listening on port '+ PORT);
}

function parseData(clientsList, message, clientSocked) {
	console.log("Parsing message: " + message +  clientSocked.id + "\n");
	var parts = message.trim().split(' ');
	if (parts[0]) parts[0] = parts[0].toUpperCase();
	let command = parts[0];
	if (command === 'REG') {
		let channel = parts[1];
		let subscriber = parts[2];
		clientsList.setChannelToClient(clientSocked.id, subscriber, channel);
		clientsList.addClient(channel, subscriber, clientSocked.id);
	}
}


function input(question, callback) {
	 var stdin = process.stdin, stdout = process.stdout;

	 stdin.resume();
	 stdout.write(question);

	 stdin.once('data', function(data) {
	   data = data.toString().trim();
	   callback(data);
	 });
}
